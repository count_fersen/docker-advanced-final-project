1. Build and push images to docker hub
   - Under /web directory run this command
        `docker build -t countfersen/web-custom:1.0 .`
        `docker push countfersen/web-custom:1.0`
   - Under /proxy directory run this command
        `docker build -t countfersen/proxy-custom:2.0 .`
        `docker push countfersen/proxy-custom:2.0`
   - Under /mysql directory run this command
        `docker build -t countfersen/mysql-custom:1.0 .`
        `docker push countfersen/mysql-custom:1.0`
   - Under /pma directory run this command
        `docker build -t countfersen/pma-custom:1.0 .`
        `docker push countfersen/pma-custom:1.0`
2. Run the following command to start the services inside docker-compose.yml
   `docker compose up`
3. Goto http://localhost:1155/ and login as root. Import the dump.sql file in volume directory.
4. Goto http://localhost:1212/ and register a user. Then use the newly created username/password to login.
5. Run the following command to deploy stack on swarm
   `docker stack deploy -c docker-compose.yml -c docker-compose-stack.yml myweb-app`
6. Goto http://localhost:1155/ and login as root. Import the dump.sql file in volume directory.
7. Goto http://localhost:1212/ and register a user. Then use the newly created username/password to login.
8. Confirm if all replicas have run successfully using `docker service ls`.
9. Confirm if all containers are healthy by checking their healthcheck status using `docker container ls`. It is shown in the status column.
